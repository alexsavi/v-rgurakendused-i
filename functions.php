<?php

function kuva_galerii($pildid){ //pisikese galerii kuvamiseks funktsioon
	global $myurl;
		include("view/main.html");
	}
	
function kuva_pilt($pildid){
	global $myurl;
	//default väärtused
	$eelmine=-1;
	$jargmine=-1;
	if (isset($_GET['id']) && is_numeric($_GET['id'])) {
		$id=$_GET['id'];
		if (!isset($pildid[$id])) header("Location: $myurl");
		if ($id>0) $eelmine=$id-1;
		if ($id<count($pildid)-1) $jargmine=$id+1;
	} else {
		 header("Location: $myurl");
	}
	include("view/pilt.html");
	}

function kuva_reg_vorm(){	// funktsioon mis kuvab registreerimise vormi
	global $myurl;		//globaalmuutuju deklareerimine	
	include("view/reg.php"); // lisab registreerimise vormi
	}
	
function kuva_ret_vorm(){
	global $myurl;
	include("view/retrieve.html");
	}
function kuva_logout(){	
	global $myurl;
	include("view/logout.php");
	}
function kuva_ul_vorm(){	
	global $myurl;
	include("view/ul.html");
	}
function kuva_admin(){
	global $myurl;
	include("view/admin_page.php");
	}
function kuva_member(){
	global $myurl;
	include("view/members_page.php");
	}
function kuva_login(){
	global $myurl;
	include("view/login.php");
	}
function kuva_info(){
	global $myurl;
	include("view/info.php");
	}
function kuva_users(){
	global $myurl;
	include("view/reg_users.php");
	}
function kuva_admin_users(){
	global $myurl;
	include("view/admin_users.php");
	}
function kuva_thanks_vorm (){
	global $myurl;
	include("view/reg_thanks.php");
	}
function signup(){ //registreerimise vormi funktsioon
	global $myurl;
	$errors=array();
	if (isset($_POST['username']) && $_POST['username']!="") {
		$username=$_POST['username'];
	} else {
		$errors[]="Kasutajanimi puudu!";
	}
	if (isset($_POST['psword1']) && $_POST['psword1']!="") {
		$p1=$_POST['psword1'];
	} else {
		$errors[]="Parool puudu, täida nõutud väli!";	
	}
	if (isset($_POST['psword2']) && $_POST['psword2']!="") {
		$p2=$_POST['psword2'];
	} else {
		$errors[]="Parool uuesti puudu, täida nõutud väli!";	
	}
		if (isset($p1) && isset($p2)){
		if ($p1==$p2) {
			$password=$p1;	
		} else {
			$errors[]="Parooliväljade sisu on erinev";	
		}
			
	}
		if (empty($errors)){
				include("view/reg.php");
		}	
	 else {
		// vead, kuva login vorm veateatega	
		include("view/reg.php");
	}
		
}

function retrieve(){ // unustasin parooli funktsioon
	global $myurl;
	$errors=array();
	$username="";
	$email="";
	if (isset($_POST['username']) && $_POST['username']!="") {
		$username=$_POST['username'];
	} else {
		$errors[]="Kasutajanimi puudu!";
	}
	if (isset($_POST['email']) && $_POST['email']!="") {
		$email=$_POST['email'];
	} else {
		$errors[]="E-mail puudu, täida nõutud väli!";	
	}

	if (empty($errors)){
		// vigu polnud, kontrolli infot
		if ($username=="kasutaja" && $email=="email"){
			// sisse logimise info!	
			header("Location: $myurl?mode=success");
		} else {
			// vale info, kuva uuesti algneleht
			$errors[]="Vale info, proovi uuesti.";	
			include("view/retrieve.html");
		}	
	} else {
		// vead, kuva algne vorm veateatega	
		include("view/retrieve.html");
	}

}
?>