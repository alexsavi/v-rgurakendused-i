-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 27, 2014 at 08:14 PM
-- Server version: 5.5.37
-- PHP Version: 5.3.10-1ubuntu3.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `asavi_users`
--

CREATE TABLE IF NOT EXISTS `asavi_users` (
  `user_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(40) NOT NULL,
  `email` varchar(50) NOT NULL,
  `psword` char(40) NOT NULL,
  `registration_date` datetime DEFAULT NULL,
  `user_level` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `asavi_users`
--

INSERT INTO `asavi_users` (`user_id`, `fname`, `lname`, `email`, `psword`, `registration_date`, `user_level`) VALUES
(8, 'Tere', 'Tali', 'asdasd123@gmail.com', 'c8499454bada15f6d76bbf8cf133960f93f9b4eb', '2014-05-26 13:36:17', 1),
(9, 'asd', 'ads', 'asd@hot.ee', '3ddc27ef5a7fe43a98fb703ae17c5466516903bf', '2014-05-26 14:34:58', 0),
(10, 'Valentsio', 'Malentsjus', 'malekabekäpp@hot.ee', 'c8499454bada15f6d76bbf8cf133960f93f9b4eb', '2014-05-27 14:25:27', 0),
(14, 'Marie', 'Beponnerosmoasdmaosdmaosdmasodmasodmoadm', 'asdasd123@hot.ee', 'c8499454bada15f6d76bbf8cf133960f93f9b4eb', '2014-05-27 14:39:45', 0),
(15, 'Marie', 'Beponnerosmoasdmaosdmaosdmasodmasodmoadm', 'asdasd123@hot.ee', '7e240de74fb1ed08fa08d38063f6a6a91462a815', '2014-05-27 14:42:26', 0),
(16, 'Marie', 'Beponnerosmoasdmaosdmaosdmasodmasodmoadm', 'asdasd123@hot.ee', '85136c79cbf9fe36bb9d05d0639c70c265c18d37', '2014-05-27 14:46:21', 0),
(17, 'Admin', 'Admin', 'Admin@admin.com', 'f865b53623b121fd34ee5426c792e5c33af8c227', '2014-05-27 15:20:37', 1),
(18, 'Fekk', 'Tekk', 'asdasd123@hot.ee', 'c8499454bada15f6d76bbf8cf133960f93f9b4eb', '2014-05-27 16:40:43', 0),
(19, 'Tere', 'Tore', 'asd@hotmail.com', 'c8499454bada15f6d76bbf8cf133960f93f9b4eb', '2014-05-27 17:15:39', 0),
(20, 'Olen', 'Maasikas', 'Maasikas@Olen.com', 'c8499454bada15f6d76bbf8cf133960f93f9b4eb', '2014-05-27 18:53:29', 0),
(21, 'Tavaline', 'Kasutaja', 'tavaline@hot,ee', 'c8499454bada15f6d76bbf8cf133960f93f9b4eb', '2014-05-27 19:43:22', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
