<!doctype html>
<html lang=en>
<head>
<title>Register page</title>
<meta charset=utf-8>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<div id="container">

<div id="content"><!--Contenti algus. -->
<p>
<?php
// loob insert käskluse abil kirje kasutajate tabelisse.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
$errors = array(); // Kui tekib viga
// Kontrollib kas eesnimi on sisestatud
if (empty($_POST['fname'])) {
$errors[] = 'Palun sisesta oma eesnimi ning proovi uuesti.';
}
else { $fn = trim($_POST['fname']);
}
// Kontrollib kas perekonnanimi on sisestatud
if (empty($_POST['lname'])) {
$errors[] = 'Palun sisesta oma perekonnanimi ning proovi uuesti.';
}
else { $ln = trim($_POST['lname']);
}
// Kontrollib kas email on sisestatud
if (empty($_POST['email'])) {
$errors[] = 'Palun sisesta oma email ning proovi uuesti.';
}
else { $e = trim($_POST['email']);
}
// Kontrollib kas paroolid kattuvad
if (!empty($_POST['psword1'])) {
if ($_POST['psword1'] != $_POST['psword2']) {
$errors[] = 'Paroolid ei kattu. Proovi uuesti.';
}
else { $p = trim($_POST['psword1']);
}
}
else { $errors[] = 'Sisesta parool ning proovi uuesti.';
}
if (empty($errors)) { // Kui probleeme ei tekkinud, registreeritakse kasutaja andmebaasi
require ('db_connect.php'); // Ühendub andmebaasiga
// Loob päringu
$q = "INSERT INTO asavi_users (user_id, fname, lname, email, psword, registration_date)
VALUES (' ', '$fn', '$ln', '$e', SHA1('$p'), NOW() )";
$result = @mysqli_query ($dbcon, $q); // Jooksutab päringu
if ($result) { // Kui kõik läks hästi
header ("location: ?mode=thanks");
exit();
}
else { // Kui esinesid vead
echo '<h2>Süsteemiviga</h2>
<p class="error">Registreerimine ebaõnnestus tehnilise vea tõttu. Vabandame ebamugavuste pärast.</p>';
// Debug the message:
echo '<p>' . mysqli_error($dbcon) . '<br><br>Query: ' . $q . '</p>';
} // Lõpetab if klausli
mysqli_close($dbcon); // Lõpetab andmebaasi ühenduse
// Lisab jaluse ning skripti
exit();
}
else { // Kuvab vead
echo '<h2>Error!</h2>
<p class="error">Registreerimisel esinesid vead:<br>';
foreach ($errors as $msg) { // Kuvab kõik vead
echo " - $msg<br>\n";
}
echo '</p><h3>Palun proovi uuesti.</h3><p><br></p>';
}// If klausli lõpp
} // main submit koodi lõpp
?>
<h2>Registreeru</h2> 
<!--Registreerimise vorm-->
<form action="?mode=signup" method="post">
<p><data class="data" for="fname">Eesnimi:</data>
<input id="fname" type="text" name="fname" size="30" maxlength="30"
value="<?php if (isset($_POST['fname'])) echo $_POST['fname']; ?>"></p>
<p><data class="data" for="lname">Perekonnanimi:</data>
<input id="lname" type="text" name="lname" size="30" maxlength="40"
value="<?php if (isset($_POST['lname'])) echo $_POST['lname']; ?>"></p>
<p><data class="data" for="email">Email aadress:</data>
<input id="email" type="text" name="email" size="30" maxlength="60"
value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>" > </p>
<p><data class="data" for="psword1">Parool:</data>
<input id="psword1" type="password" name="psword1" size="12" maxlength="12"
value="<?php if (isset($_POST['psword1'])) echo $_POST['psword1']; ?>"</br> Kuni 12 tähemärki</br>
<p><data class="data" for="psword2">Korda parooli:</data>
<input id="psword2" type="password" name="psword2" size="12" maxlength="12"
value="<?php if (isset($_POST['psword2'])) echo $_POST['psword2']; ?>" ></p>
<p><input id="submit" type="submit" name="submit" value="Register"></p>
</form><!-- Lehe sisu lõpp -->
<?php include ('view/foot.html'); ?></p>
</div>
</div>
</body>
</html>