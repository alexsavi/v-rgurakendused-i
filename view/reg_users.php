<!doctype html>
<html lang=en>
<head>
<title>Registreeritud kasutajad</title>
<meta charset=utf-8>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<div id="container">
<div id="content"><!-- Kasutajalehe sisu algus -->
<h2>Lehel registreerunud kasutajad</h2>
<form action="?mode=users" method="POST">
<?php
// php skript mis ühendub andmebaasiga ning küsib sealt kasutajate tabeli andmed
require('db_connect.php'); // andmebaasi ühendumine
// päringu loomine
$q = "SELECT CONCAT(lname, ', ', fname) AS name,
DATE_FORMAT(registration_date, '%M %d, %Y  '  ' kell %H:%i') AS regdat FROM asavi_users
ORDER BY registration_date ASC";
$result = @mysqli_query ($dbcon, $q); // päringu loomine
if ($result) { // Kui päring korras, kuvab tabeli
echo '<table>
<tr><td><b>Nimi</b></td><td><b>Registreerimise kuupäev</b></td></tr>';
// Küsib ning kuvab tulemused
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
echo '<tr><td>' . $row['name'] . '</td><td>' . $row['regdat'] . '</td></tr>'; }
echo '</table>'; // sulgeb tabeli ning kontrollib kas tabel on kuvamiseks valmis
mysqli_free_result ($result); // Vabastab mälu
} else { // Kui tekkis viga
// Veateade
echo '<p class="error">Hetkel ei ole kasutajate kuvamine kahjuks võimalik. Vabandame ebamugavuste pärast.</p>';
echo '<p>' . mysqli_error($dbcon) . '<br><br>Query: ' . $q . '</p>';
} // if käsu lõpp
mysqli_close($dbcon); // sulgeb ühenduse


?>
<form action="pdf_loomine.php" method="POST">
<input name="loo_pdf" type="text"/>
<input name="submit" type="submit" value="Loo PDF" />
</form> 
</form>
</div><!-- Kasutajate tabelilehe lõpp -->
</div>
</body>
</html>