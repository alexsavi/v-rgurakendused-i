<!doctype html>

<html lang=en>
<head>
<title>Registreerimise kinnitus</title>
<meta charset=utf-8>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<div id="container">
<?php include_once("head.html"); ?>
<div id="content"><!-- Thank you page contenti algus -->
<h2>Aitäh end registreerimast minu lehele! Kui soovid näha kes on veel end minu lehel registreerinud, logi end sisse ning vajuta lingil "Kasutajad"</h2>
<p>Kohe suunab sind brauser sisselogimise lehele..</p>
<!-- End of the thank you page-content. -->
</div>
</div>
<?php header("Refresh: 4; URL=?mode=login");
//* header("Location: ../kontroller.php?mode=login"); /* PEAKS brauseri suunama */
exit();?>

</body>
</html>