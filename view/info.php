<!doctype html>
<html lang=en>
<head>
<title>Info</title>
<meta charset=utf-8>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<div id=info>
<h1>Lühiülevaade lehekülje kohta</h1>
<p>* Leht on loodud suuresti tänu Tiia Tänava praktikumidele, kust on ka palju mõtteid võetud<br>
* Suur osa lehe funktsionaalsusest on saavutatud tänu <a href="http://www.apress.com/9781430260769">sellele</a> raamatule<br>
* Leht on alles lapsekingades, palun võtta siinkohal teada, et lehe looja on iseõppinud amatöör<br>
* Osa koodi on saadud korda ka tänu <a href="http://www.stackoverflow.com">stackoverflow.com</a> veebileheküljele ning sealt pärineb nii mõnigi pisike koodijupike, <br>
mida olen vastavalt oma lehekülje vajadustele modifitseerinud<br>
* Lehekülg kasutab phpMyAdmin andmebaasi "test", mille omanik on itkolledž<br>
* Lehekülje loomiseks on kokku läinud umbes 30 tundi ning kõige suurema osa võttis sellest ära php ning MySQL<br> 
* küsimuste korral võib kirjutada alex.savi@itcollege.ee , vastan esimesel võimalusel :)<br>

</p>
</div>
</body>
<form action="?mode=info" method="POST">
</form>
</html>