<!doctype html>
<html lang=en>
<head>
<title>The Login page</title>
<meta charset=utf-8>
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<div id="container">
<div id="content">
<form action="?mode=login" method="POST"><!-- login lehe sisu -->
<?php
// Kontrollib kas päring on tehtud vormi täitmiseks
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//ühendub andmebaasiga
require ('db_connect.php');
// valideerib emaili aadressi
if (!empty($_POST['email'])) {
$e = mysqli_real_escape_string($dbcon, $_POST['email']);
} else {
$e = FALSE;
echo '<p class="error">Unustasid sisestada oma e-maili aadressi!</p>';
}
// valideerib parooli
if (!empty($_POST['psword'])) {
$p = mysqli_real_escape_string($dbcon, $_POST['psword']);
} else {
$p = FALSE;
echo '<p class="error">Unustasid sisestada oma parooli!</p>';
}
if ($e && $p){//kui ei esinenud probleeme
// haarab vajaliku informatsiooni andmebaasi tabelist
$q = "SELECT user_id, fname, user_level FROM asavi_users WHERE (email='$e' AND psword=SHA1('$p'))";
// käivitab päringu query ning seob ta $result muutujaga
$result = mysqli_query ($dbcon, $q);
// loeb kokku read mis sobivad emaili ja parooli kombinatsiooniga
if (@mysqli_num_rows($result) == 1) {// kui mingi rida sobib sisestatud reaga siis alustab sessioonining paneb arraysse need 3 väärtust
session_start(); 
$_SESSION = mysqli_fetch_array ($result, MYSQLI_ASSOC);
$_SESSION['user_level'] = (int) $_SESSION['user_level'];
$url = ($_SESSION['user_level'] === 1) ? '?mode=admin' : '?mode=member';
header('Location: ' . $url); // Brauser laeb tavakasutaja või admini lehekülje
exit(); // Cancel the rest of the script
mysqli_free_result($result);
mysqli_close($dbcon);
} else { //Ei leitud samasugust väärtust
echo '<p class="error">Sisestatud e-mail ja parool ei kattu meie serveris olevate andmetega. <br>
Äkki peaksid registreerima end ning seejärel sisse logima.<br> 
Registreerida saab paremal üleval, vajutades nupul "Registreeri".</p>';
}
} else { // Kui tekkis probleem, kuvab errori
echo '<p class="error">Palun proovi uuesti!</p>';
}
mysqli_close($dbcon);
}
?>
<!-- form väljad -->
</form> 
<div id="loginfields">
<?php include ('login_page.php'); ?>
</div><br>
</div>
</div>
</body>
</html>