<?php
session_start();//ühendub käiva sessiooniga
// kui sessiooni ei käi, siis suunab kasutaja avalehele
if (!isset($_SESSION['user_id'])) {
header("location: ?");
exit();
//lõpetab sessiooni
}else{ //katkestab sessiooni
$_SESSION = array(); // hävitab sessiooni andmed
session_destroy(); // hävitab sessiooni
setcookie('PHPSESSID', ", time()-3600,'/', ", 0, 0);//hävitab sessiooni küpsise
header("location: ?"); 
exit();
}
?>
<form action="?mode=logout" method="POST">
</form>