<!doctype html>
<html lang=en>
<head>
<title>Admini vaade registreeritud kasutajate kohta</title>
<meta charset=utf-8>
<link rel="stylesheet" type="text/css" href="../style.css">
<style type="text/css">
p { text-align:center; }
</style>
</head>
<body>
<form action=?mode="admin_users" method="POST">
<div id="container">
<?php include("admin_header.php"); ?>
<div id="content"><!--content mis on lehe sisuks-->
<h2>Registreeritud kasutajad 100-kaupa</h2>
<p>
<?php
// tõmbab andmebaasist kogu info asavi_users
require ('db_connect.php'); // ühendub andmebaasiga
//näitab mitme kaupa kujutab lehel kasutajaid
$pagerows = 100;
if (isset($_GET['p']) && is_numeric
($_GET['p'])) {//arvutab välja mitu lk näidata
$pages=$_GET['p'];
}else{
//Otsib mitu tükki on tabelis kasutajaid
$q = "SELECT COUNT(user_id) FROM asavi_users";
$result = @mysqli_query ($dbcon, $q);
$row = @mysqli_fetch_array ($result, MYSQLI_NUM);
$records = $row[0];
//arvutab mitme lk kaupa peab näitama
if ($records > $pagerows){ //kui andmemaht ületab ühe lk kohta näitamise piiri
$pages = ceil ($records/$pagerows);
}else{
$pages = 1;
}
}
//Deklareerib millisest väärtusest alustada kuvamist
if (isset($_GET['s']) && is_numeric
($_GET['s'])) {//arvutab s väärtuse
$start = $_GET['s'];
}else{
$start = 0;
}
// kutsub välja andmebaasist väärtused
$q = "SELECT lname, fname, email, DATE_FORMAT(registration_date, '%M %d, %Y')
AS regdat, user_id FROM asavi_users ORDER BY registration_date ASC LIMIT $start, $pagerows";
$result = @mysqli_query ($dbcon, $q); // täidab välja kutsutud käsu
$members = mysqli_num_rows($result);
if ($result) { // kuvab tulemuse, kui kõik oli korras
// tabel
echo '<table>
<tr><td><b>Edit</b></td>
<td><b>Delete</b></td>
<td><b>Last Name</b></td>
<td><b>First Name</b></td>
<td><b>Email</b></td>
<td><b>Date Registered</b></td>
</tr>';
// püüab andmebaasist andmed ning kuvab need
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
echo '<tr>
<td><a href="edit_record' . $row['user_id'] . '">Edit</a></td>
<td><a href="del_record' . $row['user_id'] . '">Delete</a></td>
<td>' . $row['lname'] . '</td>
<td>' . $row['fname'] . '</td>
<td>' . $row['email'] . '</td>
<td>' . $row['regdat'] . '</td>
</tr>';
}
echo '</table>'; // tabeli sulgemine
mysqli_free_result ($result); // vabastab ressurssi
} else { // Kui miskit läks valesti ja kood ei käivitunud
// veateada
echo '<p class="error">Kahjuks ei saa hetkel kasutajaid kuvada, proovige hiljem uuesti.</p>';
// debug
echo '<p>' . mysqli_error($dbcon) . '<br><br />Query: ' . $q . '</p>';
} // if lõpp
//kuvab lõpptulemuse
$q = "SELECT COUNT(user_id) FROM asavi_users";
$result = @mysqli_query ($dbcon, $q);
$row = @mysqli_fetch_array ($result, MYSQLI_NUM);
$members = $row[0];
mysqli_close($dbcon); // Sulgeb databaasi ühenduse
echo "<h4>Kasutajaid kokku: $members</h4>";
if ($pages > 1) {
echo '<p>';
//mis lk on hetkel ees
$current_page = ($start/$pagerows) + 1;
// loob eelmise lk lingi kui see lk pole esimene lk
if ($current_page != 1) {
echo '<a href="reg_kasutajad' . ($start - $pagerows) .
'&p=' . $pages .'">Previous</a> ';
}
//loob  "next" lingi
if ($current_page != $pages) {
echo '<a href="reg_kasutajad' . ($start + $pagerows) .
'&p=' . $pages . '">Next</a> ';
}
echo '</p>';
}
?>
</div>
</div>
</body>
</html>