<?php
require_once("functions.php"); // loodud funktsioonide laadimine kontroller.php'sse

$myurl=$_SERVER['PHP_SELF']; // küsib serveri käest kontroller.php aadressi ning salvestab $myurl'i

$pildid=array( // loome piltidele array,et salvestada need mällu
  array("big"=>"big/45.jpg", "small"=>"small/45_small.jpg", "alt"=>"Tervist!"),
  array("big"=>"big/44.jpg", "small"=>"small/44_small.jpg", "alt"=>"Tervist2!"),
  );
// vaikimisi meetod on pealehe kuvamine
$mode="main";				

//kui get parameeter mode on antud, siis jätab see selle meelde				
if (isset($_GET['mode']) && $_GET['mode']!=""){
	$mode=$_GET['mode'];
	}
	
	
//lisame kujunduse päise
include_once("view/head.html");

switch($mode){
case "retrieve1":
	kuva_ret_vorm();
break;
case "member":
	kuva_member();
break;
case "admin":
	kuva_admin();
break;
case "logout":
	kuva_logout();
break;
case "info":
	kuva_info();
break;		
case "login":
	kuva_login();
break;
case "admin_users":
	kuva_admin_users();
break;
case "thanks":
	kuva_thanks_vorm();
break;
case "parool_tagasi":
	retrieve();
break;
case "reg_vorm":
	kuva_reg_vorm();
break;
case "upload":
	kuva_ul_vorm();
break;						
case "view":
	kuva_pilt($pildid);
break;

case "signup":
	signup();
break;
case "users":
	kuva_users();
break;
case "success": 
	echo "<p>Oled sisse logitud!/p>";
	kuva_galerii($pildid);
break;
default:
	kuva_galerii($pildid);
}
   include_once("view/foot.html");
   
   ?>